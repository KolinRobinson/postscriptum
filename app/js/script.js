const swiper = new Swiper('.preview__slider', {
	// Optional parameters
	direction: 'vertical',
	pagination: {
		el: '.preview__inner .swiper-pagination',
		clickable: true,
		renderBullet: function (index, className) {
			return `<span class="dot swiper-pagination-bullet">${2019 - index}</span>`;
		}
	},

	navigation: {
		nextEl: '.preview__inner .swiper-button-next',
		prevEl: '.preview__inner .swiper-button-prev',
	}
});

function getMatrix(element) {
	const values = element.style.transform.split(/\w+\(|\);?/);
	const transform = values[1].split(/,\s?/g).map(item => item.split('px')[0]);

	return {
		x: +transform[0],
		y: +transform[1],
		z: +transform[2]
	};
}

let timeOutOne = null;
let timeOutTwo = null;
let sliderContainer = document.querySelector('.preview__inner .swiper-wrapper')
let beforeTrans = 0
let trans = 0

swiper.on('slideNextTransitionStart', function () {
	trans = getMatrix(sliderContainer).y
	sliderContainer.style.transform = `translate3d(0px, ${beforeTrans + 100}px, 0px)`

	timeOutOne = setTimeout(() => {
		sliderContainer.style.transform = `translate3d(0px, ${trans - 50}px, 0px)`
	}, 250)
	timeOutTwo = setTimeout(() => {
		sliderContainer.style.transform = `translate3d(0px, ${trans}px, 0px)`
	}, 750)
	beforeTrans = trans
});

swiper.on('slidePrevTransitionStart', function () {
	trans = getMatrix(sliderContainer).y
	sliderContainer.style.transform = `translate3d(0px, ${beforeTrans - 100}px, 0px)`

	timeOutOne = setTimeout(() => {
		sliderContainer.style.transform = `translate3d(0px, ${trans + 50}px, 0px)`
	}, 250)
	timeOutTwo = setTimeout(() => {
		sliderContainer.style.transform = `translate3d(0px, ${trans}px, 0px)`
	}, 750)
	beforeTrans = trans
});

let yers = [2018, 2021]
const swiperTwo = new Swiper('.our__swiper', {
	navigation: {
		nextEl: '.our__inner .swiper-button-next',
		prevEl: '.our__inner .swiper-button-prev',
	},
	pagination: {
		el: '.our__inner .swiper-pagination',
		clickable: true
	},
})


document.querySelector('.swiper-line__full').style.width = `${100 / swiperTwo.slides.length - 20}%`

swiperTwo.on('slideChange', function () {
	document.querySelector('.swiper-line__full').style.width = `${(100 / swiperTwo.slides.length - 20) * (swiperTwo.activeIndex + 1)}%`
});

let sliders = document.querySelectorAll('.stages__slide')
let index = 0
let arrowPrev = document.querySelectorAll('.stages-button-prev')[0]
let arrowNext = document.querySelectorAll('.stages-button-next')[0]

sliders[index].classList.add('active')

arrowNext.addEventListener('click', ()=>{
	sliders.forEach(item=>{
		item.classList.remove('active')
		item.classList.remove('prev-slide')
		item.classList.remove('next-slide')
	})
	index++
	if(index > sliders.length -1){
		index = 0
	}
	sliders[index].classList.add('active')
	sliders[index - 1].classList.add('next-slide')
	changeCurrentIndex()
})


arrowPrev.addEventListener('click', ()=>{
	sliders.forEach(item=>{
		item.classList.remove('active')
		item.classList.remove('prev-slide')
		item.classList.remove('next-slide')
	})
	index--
	if(index < 0){
		index = sliders.length -1
	}
	sliders[index].classList.add('active')
	sliders[index + 1].classList.add('prev-slide')
	changeCurrentIndex()
})

function changeCurrentIndex(){
	document.querySelector('.stages__current').innerHTML = `${index + 1} / 9`
}
document.body.addEventListener('click', (e) => {
	if (!document.body.classList.contains('active-menu') && e.target.closest('.menu-btn')){
		document.body.classList.add('active-menu')
	} else if(document.body.classList.contains('active-menu') && !e.target.closest('.header__nav')){
		document.body.classList.remove('active-menu')
	}
})